# Local_docker_registry

## Setup docker client
1. Add following in `/etc/docker/daemon.json`
```
sudo vim /etc/docker/daemon.json

(in vim, add following)
{
  "live-restore": true,
  "group": "dockerroot",
  "insecure-registries": ["<IP>:5000"]
}
```

2. Restart docker service
```
sudo systemctl daemon-reload
sudo systemctl restart docker
```

3. Tag docker image
```
docker tag ubuntu:18.04 <IP>:5000/ubuntu:18.04
```

4. Push to remote
```
docker push <IP>:5000/ubuntu:18.04
```

## Setup Registry web ui
1. Use docker-compose to set up
```
docker-compose up -d
```

2. For more info, see examples in [Joxit/docker-registry-ui](https://github.com/Joxit/docker-registry-ui)

## Reference
1. [Joxit/docker-registry-ui](https://github.com/Joxit/docker-registry-ui)
2. [Day7：建立 private 的 Docker Registry](https://ithelp.ithome.com.tw/articles/10191213)
